<?php

  class Comite extends CI_Model
  {

    function __construct()
    {
      parent::__construct();
    }


    function insertar($datos){
        //Active record en CodeIgniter
        return $this->db->insert('comite',$datos);
    }

    function obtenerTodos(){
        $listadoAutores=$this->db->get("comite");
        if ($listadoAutores->num_rows()>0) {

          return $listadoAutores->result();
        } else {
          return false;
        }


    }

    public function borrar($id_com)
    {
      $this->db->where("id_com",$id_com);
      return $this->db->delete('comite');
    }

     //Funcion para consultar un seminario especifico
     public function ObtenerporId($id_com)
     {
       $this->db->where("id_com",$id_com);
       $autor=$this->db->get("comite");
       if ($autor->num_rows()>0){
         return $autor->row();
       }
        return false;
 
     }
     //funcion para actualizar un instructor
     public function actualizar($id_com,$datos)
     {
       $this->db->where("id_com",$id_com);
       return $this->db->update("comite",$datos);
     }


  }




 ?>
