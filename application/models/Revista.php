<?php

  class Revista extends CI_Model
  {

    function __construct()
    {
      parent::__construct();
    }


    function insertar($datos){
        //Active record en CodeIgniter
        return $this->db->insert('revista',$datos);
    }

    function obtenerTodos(){
        $listadoAutores=$this->db->get("revista");
        if ($listadoAutores->num_rows()>0) {

          return $listadoAutores->result();
        } else {
          return false;
        }


    }

    public function borrar($id_rev)
    {
      $this->db->where("id_rev",$id_rev);
      return $this->db->delete('revista');
    }

     //Funcion para consultar un seminario especifico
     public function ObtenerporId($id_rev)
     {
       $this->db->where("id_rev",$id_rev);
       $autor=$this->db->get("revista");
       if ($autor->num_rows()>0){
         return $autor->row();
       }
        return false;
 
     }
     //funcion para actualizar un instructor
     public function actualizar($id_rev,$datos)
     {
       $this->db->where("id_rev",$id_rev);
       return $this->db->update("revista",$datos);
     }


  }




 ?>
