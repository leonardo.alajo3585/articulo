<?php

  class Articulo extends CI_Model
  {

    function __construct()
    {
      parent::__construct();
    }


    function insertar($datos){
        //Active record en CodeIgniter
        return $this->db->insert('articulo',$datos);
    }

    function obtenerTodos(){
        $listadoAutores=$this->db->get("articulo");
        if ($listadoAutores->num_rows()>0) {

          return $listadoAutores->result();
        } else {
          return false;
        }


    }

    public function borrar($id_art)
    {
      $this->db->where("id_art",$id_art);
      return $this->db->delete('articulo');
    }

     //Funcion para consultar un seminario especifico
     public function ObtenerporId($id_art)
     {
       $this->db->where("id_art",$id_art);
       $autor=$this->db->get("articulo");
       if ($autor->num_rows()>0){
         return $autor->row();
       }
        return false;
 
     }
     //funcion para actualizar un instructor
     public function actualizar($id_art,$datos)
     {
       $this->db->where("id_art",$id_art);
       return $this->db->update("articulo",$datos);
     }


  }




 ?>
