<?php

  class Colaborador extends CI_Model
  {

    function __construct()
    {
      parent::__construct();
    }


    function insertar($datos){
        //Active record en CodeIgniter
        return $this->db->insert('colaboradores',$datos);
    }

    function obtenerTodos(){
        $listadoAutores=$this->db->get("colaboradores");
        if ($listadoAutores->num_rows()>0) {

          return $listadoAutores->result();
        } else {
          return false;
        }


    }

    public function borrar($id_col)
    {
      $this->db->where("id_col",$id_col);
      return $this->db->delete('colaboradores');
    }

     //Funcion para consultar un seminario especifico
     public function ObtenerporId($id_col)
     {
       $this->db->where("id_col",$id_col);
       $autor=$this->db->get("colaboradores");
       if ($autor->num_rows()>0){
         return $autor->row();
       }
        return false;
 
     }
     //funcion para actualizar un instructor
     public function actualizar($id_col,$datos)
     {
       $this->db->where("id_col",$id_col);
       return $this->db->update("colaboradores",$datos);
     }


  }




 ?>
