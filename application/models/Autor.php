<?php

  class Autor extends CI_Model
  {

    function __construct()
    {
      parent::__construct();
    }


    function insertar($datos){
        //Active record en CodeIgniter
        return $this->db->insert('autores',$datos);
    }

    function obtenerTodos(){
        $listadoAutores=$this->db->get("autores");
        if ($listadoAutores->num_rows()>0) {

          return $listadoAutores->result();
        } else {
          return false;
        }


    }

    public function borrar($id_aut)
    {
      $this->db->where("id_aut",$id_aut);
      return $this->db->delete('autores');
    }

     //Funcion para consultar un seminario especifico
     public function ObtenerporId($id_aut)
     {
       $this->db->where("id_aut",$id_aut);
       $autor=$this->db->get("autores");
       if ($autor->num_rows()>0){
         return $autor->row();
       }
        return false;
 
     }
     //funcion para actualizar un instructor
     public function actualizar($id_aut,$datos)
     {
       $this->db->where("id_aut",$id_aut);
       return $this->db->update("autores",$datos);
     }


  }




 ?>
