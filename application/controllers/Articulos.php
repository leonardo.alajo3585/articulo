<?php


class Articulos extends CI_Controller {

    function __construct()
    {
      parent::__construct();
      //Cargar el modelo
      $this->load->model('Articulo');
      $this->load->model('Revista');
      $this->load->model('Autor');
      
    }

	public function index()
	{
        $data['articulos'] = $this->Articulo->obtenerTodos();
		$this->load->view('header');
        $this->load->view('articulos/index',$data);
		$this->load->view('footer');
	}

    public function nuevo()
	{
        $data['revistas']= $this->Revista->obtenerTodos();
        $data['autores']= $this->Autor->obtenerTodos();
		$this->load->view('header');
        $this->load->view('articulos/nuevo',$data);
		$this->load->view('footer');
	}

    public function editar($id_art)
	{
        $data["articuloEditar"]=$this->Articulo->ObtenerporId($id_art);
        $data['revistas']= $this->Revista->obtenerTodos();
        $data['autores']= $this->Autor->obtenerTodos();
		$this->load->view('header');
        $this->load->view('articulos/editar',$data);
		$this->load->view('footer');
	}

    public function guardar(){ 
        $datosNuevoArticulo = array (
            "tema_art"=>$this->input->post('tema_art'),
            "fecha_art"=>$this->input->post('fecha_art'),
            "resumen_art"=>$this->input->post('resumen_art'),
            "volumen_art"=>$this->input->post('volumen_art'),
            "palabras_claves_art"=>$this->input->post('palabras_claves_art'),
            "fk_id_rev"=>$this->input->post('fk_id_rev'),
            "fk_id_aut"=>$this->input->post('fk_id_aut'),
        );

        if ($this->Articulo->insertar($datosNuevoArticulo))
        {
          redirect ('articulos/index');
        }else {
          echo "<h1>Error al insertar</h1>";
        }
     
    }

    public function eliminar($id_art)
    {
      if ($this->Articulo->borrar($id_art)) {
        redirect ('articulos/index');
      } else {
        echo "Error al guardar";
      }
 
    }


    public function procesoActualizar(){
        $datosEditarArticulo = array (
            "tema_art"=>$this->input->post('tema_art'),
            "fecha_art"=>$this->input->post('fecha_art'),
            "resumen_art"=>$this->input->post('resumen_art'),
            "volumen_art"=>$this->input->post('volumen_art'),
            "palabras_claves_art"=>$this->input->post('palabras_claves_art'),
            "fk_id_rev"=>$this->input->post('fk_id_rev'),
            "fk_id_aut"=>$this->input->post('fk_id_aut'),
        );
        $id_art=$this->input->post("id_art");
        if($this->Articulo->actualizar($id_art,$datosEditarArticulo)) {
            redirect ('articulos/index');
     
          } else {
           
            echo "Error al editar";
          }redirect ('articulos/index');
    }
}


?>

