<?php


class Colaboradores extends CI_Controller {

    function __construct()
    {
      parent::__construct();
      //Cargar el modelo
      $this->load->model('Colaborador');

    }

	public function index()
	{
        $data['colaboradores'] = $this->Colaborador->obtenerTodos();
		$this->load->view('header');
        $this->load->view('colaboradores/index',$data);
		$this->load->view('footer');
	}

    public function nuevo()
	{
		$this->load->view('header');
        $this->load->view('colaboradores/nuevo');
		$this->load->view('footer');
	}

    public function editar($id_col)
	{
        $data["colaboradorEditar"]=$this->Colaborador->ObtenerporId($id_col);
		$this->load->view('header');
        $this->load->view('colaboradores/editar',$data);
		$this->load->view('footer');
	}

    public function guardar(){
        $datosNuevoColaborador = array (
            "nombres_col"=>$this->input->post('nombres_col'),
            "apellidos_col"=>$this->input->post('apellidos_col'),
            "telefono_col"=>$this->input->post('telefono_col'),

        );

        if ($this->Colaborador->insertar($datosNuevoColaborador))
        {
          redirect ('colaboradores/index');
        }else {
          echo "<h1>Error al insertar</h1>";
        }

    }

    public function eliminar($id_col)
    {
      if ($this->Colaborador->borrar($id_col)) {
        redirect ('colaboradores/index');
      } else {
        echo "Error al guardar";
      }

    }


    public function procesoActualizar(){
        $datosEditarColaborador = array (
            "nombres_col"=>$this->input->post('nombres_col'),
            "apellidos_col"=>$this->input->post('apellidos_col'),
            "telefono_col"=>$this->input->post('telefono_col'),
        );
        $id_col=$this->input->post("id_col");
        if($this->Colaborador->actualizar($id_col,$datosEditarColaborador)) {
            redirect ('colaboradores/index');

          } else {

            echo "Error al editar";
          }redirect ('colaboradores/index');
    }
    public function descargar_pdf($id_col)
    {
        // Obtener los datos del colaborador por su ID
        $colaborador = $this->Colaborador->obtenerPorId($id_col);

        // Lógica para generar el PDF del colaborador con los datos obtenidos

        // Ejemplo de cómo devolver un PDF como descarga
        $pdf_file = 'nombre_del_archivo.pdf';
        header("Content-Type: application/pdf");
        header("Content-Disposition: attachment; filename=\"$pdf_file\"");
        // Generar y enviar el PDF al navegador
        readfile('ruta/al/archivo.pdf');
    }
}


?>
