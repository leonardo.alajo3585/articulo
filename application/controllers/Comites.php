<?php


class Comites extends CI_Controller {

    function __construct()
    {
      parent::__construct();
      //Cargar el modelo
      $this->load->model('Comite');
     
    }

	public function index()
	{
        $data['comites'] = $this->Comite->obtenerTodos();
		$this->load->view('header');
        $this->load->view('comites/index',$data);
		$this->load->view('footer');
	}

    public function nuevo()
	{
		$this->load->view('header');
        $this->load->view('comites/nuevo');
		$this->load->view('footer');
	}

    public function editar($id_com)
	{
        $data["comiteEditar"]=$this->Comite->ObtenerporId($id_com);
		$this->load->view('header');
        $this->load->view('comites/editar',$data);
		$this->load->view('footer');
	}

    public function guardar(){ 
        $datosNuevoComite = array (
            "nombre_com"=>$this->input->post('nombre_com'),
            "director_com"=>$this->input->post('director_com'),

        );

        if ($this->Comite->insertar($datosNuevoComite))
        {
          redirect ('comites/index');
        }else {
          echo "<h1>Error al insertar</h1>";
        }
     
    }

    public function eliminar($id_com)
    {
      if ($this->Comite->borrar($id_com)) {
        redirect ('comites/index');
      } else {
        echo "Error al guardar";
      }
 
    }


    public function procesoActualizar(){
        $datosEditarComite = array (
            "nombre_com"=>$this->input->post('nombre_com'),
            "director_com"=>$this->input->post('director_com'),
        );
        $id_com=$this->input->post("id_com");
        if($this->Comite->actualizar($id_com,$datosEditarComite)) {
            redirect ('comites/index');
     
          } else {
           
            echo "Error al editar";
          }
    }
}


?>