<?php


class Autores extends CI_Controller {

    function __construct()
    {
      parent::__construct();
      //Cargar el modelo
      $this->load->model('Autor');

    }

	public function index()
	{
        $data['autores'] = $this->Autor->obtenerTodos();
		$this->load->view('header');
        $this->load->view('autores/index',$data);
		$this->load->view('footer');
	}

    public function nuevo()
	{
		$this->load->view('header');
        $this->load->view('autores/nuevo');
		$this->load->view('footer');
	}

    public function editar($id_aut)
	{
        $data["autorEditar"]=$this->Autor->ObtenerporId($id_aut);
		$this->load->view('header');
        $this->load->view('autores/editar',$data);
		$this->load->view('footer');
	}

    public function guardar(){
        $datosNuevoAutor = array (
            "nombres_aut"=>$this->input->post('nombres_aut'),
            "apellidos_aut"=>$this->input->post('apellidos_aut'),
            "cedula_aut"=>$this->input->post('cedula_aut'),
            "telefono_aut"=>$this->input->post('telefono_aut'),
            "direccion_aut"=>$this->input->post('direccion_aut'),
            "correo_aut"=>$this->input->post('correo_aut'),
        );

        if ($this->Autor->insertar($datosNuevoAutor))
        {
          redirect ('autores/index');
        }else {
          echo "<h1>Error al insertar</h1>";
        }

    }

    public function eliminar($id_aut)
    {
      if ($this->Autor->borrar($id_aut)) {
        redirect ('autores/index');
      } else {
        echo "Error al guardar";
      }

    }


    public function procesoActualizar(){
        $datosEditarAutor = array (
            "nombres_aut"=>$this->input->post('nombres_aut'),
            "apellidos_aut"=>$this->input->post('apellidos_aut'),
            "cedula_aut"=>$this->input->post('cedula_aut'),
            "telefono_aut"=>$this->input->post('telefono_aut'),
            "direccion_aut"=>$this->input->post('direccion_aut'),
            "correo_aut"=>$this->input->post('correo_aut'),
        );
        $id_aut=$this->input->post("id_aut");
        if($this->Autor->actualizar($id_aut,$datosEditarAutor)) {
            redirect ('autores/index');

          } else {

            echo "Error al editar";
          }redirect ('autores/index');
    }
}


?>
