<?php


class Revistas extends CI_Controller {

    function __construct()
    {
      parent::__construct();
      //Cargar el modelo
      $this->load->model('Revista');
      $this->load->model('Comite');
      $this->load->model('Colaborador');
    }

	public function index()
	{
     $data['revistas'] = $this->Revista->obtenerTodos();
		$this->load->view('header');
    $this->load->view('revistas/index',$data);
		$this->load->view('footer');
	}

    public function nuevo()
	{
        $data['colaboradores']= $this->Colaborador->obtenerTodos();
        $data['comites']= $this->Comite->obtenerTodos();
		$this->load->view('header');
    $this->load->view('revistas/nuevo',$data);
		$this->load->view('footer');
	}

    public function editar($id_rev)
	{
        $data["revistaEditar"]=$this->Revista->ObtenerporId($id_rev);
        $data['colaboradores']= $this->Colaborador->obtenerTodos();
        $data['comites']= $this->Comite->obtenerTodos();
		$this->load->view('header');
        $this->load->view('revistas/editar',$data);
		$this->load->view('footer');
	}

    public function guardar(){ 
        $datosNuevoRevista = array (
            "nombre_rev"=>$this->input->post('nombre_rev'),
            "direccion_rev"=>$this->input->post('direccion_rev'),
            "telefono_rev"=>$this->input->post('telefono_rev'),
            "correo_rev"=>$this->input->post('correo_rev'),
            "fk_id_col"=>$this->input->post('fk_id_col'),
            "fk_id_com"=>$this->input->post('fk_id_com'),
        );

        if ($this->Revista->insertar($datosNuevoRevista))
        {
          redirect ('revistas/index');
        }else {
          echo "<h1>Error al insertar</h1>";
        }
     
    }

    public function eliminar($id_rev)
    {
      if ($this->Revista->borrar($id_rev)) {
        redirect ('revistas/index');
      } else {
        echo "Error al guardar";
      }
 
    }


    public function procesoActualizar(){
        $datosEditarRevista = array (
            "nombre_rev"=>$this->input->post('nombre_rev'),
            "direccion_rev"=>$this->input->post('direccion_rev'),
            "telefono_rev"=>$this->input->post('telefono_rev'),
            "correo_rev"=>$this->input->post('correo_rev'),
            "fk_id_col"=>$this->input->post('fk_id_col'),
            "fk_id_com"=>$this->input->post('fk_id_com'),
        );
        $id_rev=$this->input->post("id_rev");
        if($this->Revista->actualizar($id_rev,$datosEditarRevista)) {
            redirect ('revistas/index');
     
          } else {
           
            echo "Error al editar";
          }redirect ('revistas/index');
    }
}


?>

