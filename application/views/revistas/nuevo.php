<section class="section-padding section-bg">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-12">
                <h3 class="mb-4 pb-2">Datos del Colaborador</h3>
            </div>
            <div class="col-lg-12 col-12">
                <form method="post" class="custom-form contact-form" role="form" action="<?php echo site_url(); ?>/revistas/guardar" id="form-revista">
                    <div class="row">
                        <div class="col-lg-4 col-md-12 col-12">
                            <div>
                                <input type="text" name="nombre_rev" id="nombre_rev" class="form-control" placeholder="Nombre de la revista" required="">
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div>
                                <input type="text" name="direccion_rev" id="direccion_rev" class="form-control" placeholder="Dirección de la revista" required="">
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div>
                                <input type="number" name="telefono_rev" id="telefono_rev" class="form-control" placeholder="Teléfono de la revista" required="">
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div>
                                <input type="email" name="correo_rev" id="correo_rev" class="form-control" placeholder="Correo de la revista" required="">
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div>
                                <select name="fk_id_com" id="fk_id_com" class="form-control">
                                    <?php if ($comites): ?>
                                        <?php foreach ($comites as $c): ?>
                                            <option value="<?php echo $c->id_com ?>"> <?php echo $c->nombre_com ?></option>
                                        <?php endforeach;?>
                                    <?php else :?>
                                        <option value="">No hay datos</option>
                                    <?php endif; ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div>
                                <select name="fk_id_col" id="fk_id_col" class="form-control">
                                    <?php if ($colaboradores): ?>
                                        <?php foreach ($colaboradores as $c): ?>
                                            <option value="<?php echo $c->id_col ?>"> <?php echo $c->nombres_col ?></option>
                                        <?php endforeach;?>
                                    <?php else :?>
                                        <option value="">No hay datos</option>
                                    <?php endif; ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-4 col-12 ms-auto">
                            <button type="submit" class="form-control">Guardar</button>
                        </div>
                    </div>
                </form>
            </div>
            <!-- Script de validación -->
            <script>
                $(document).ready(function () {
                    // Inicializar las validaciones del formulario
                    $("#form-revista").validate({
                        // Definir las reglas de validación para cada campo
                        rules: {
                            nombre_rev: "required",
                            direccion_rev: "required",
                            telefono_rev: {
                                required: true,
                                digits: true
                            },
                            correo_rev: {
                                required: true,
                                email: true
                            },
                            fk_id_com: "required",
                            fk_id_col: "required"
                        },
                        // Definir los mensajes de error para cada campo
                        messages: {
                            nombre_rev: "Por favor, ingrese el nombre de la revista",
                            direccion_rev: "Por favor, ingrese la dirección de la revista",
                            telefono_rev: {
                                required: "Por favor, ingrese el teléfono de la revista",
                                digits: "Por favor, ingrese solo dígitos en el teléfono de la revista"
                            },
                            correo_rev: {
                                required: "Por favor, ingrese el correo de la revista",
                                email: "Por favor, ingrese un correo electrónico válido"
                            },
                            fk_id_com: "Por favor, seleccione un comité",
                            fk_id_col: "Por favor, seleccione un colaborador"
                        },
                        // Definir el lugar donde se mostrarán los mensajes de error
                        errorPlacement: function(error, element) {
                            error.appendTo(element.parent());
                            error.addClass('text-danger'); // Agregar la clase para el color rojo
                        }
                    });
                });
            </script>
        </div>
    </div>
</section>
