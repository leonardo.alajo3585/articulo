<section class="section-padding section-bg">
                <div class="container">
                    <div class="row">

                        <div class="col-lg-12 col-12">
                            <h3 class="mb-4 pb-2">Datos del Colaborador</h3>
                        </div>

                        <div class="col-lg-12 col-12">
                            <form  method="post" class="custom-form contact-form" role="form" action="<?php echo site_url(); ?>/revistas/procesoActualizar">
                                <div class="row">
                                <input type="hidden" name="id_rev" id="id_rev" value="<?php echo $revistaEditar->id_rev ?>" >

                                    <div class="col-lg-4 col-md-12 col-12">
                                        <div class="form-floating">
                                            <input type="text" name="nombre_rev" id="nombre_rev" class="form-control" value="<?php echo $revistaEditar->nombre_rev ?>" placeholder="Nombre de la revista" required="">
                                            
                                            <label for="floatingInput">Nombres de la revista </label>
                                        </div>
                                      
                                    </div>

                                    <div class="col-lg-4">
                                        <div class="form-floating">
                                                <input type="text" name="direccion_rev" id="direccion_rev"  value="<?php echo $revistaEditar->direccion_rev ?>" class="form-control" placeholder="Direccion de la revista" required="">
                                                
                                                <label for="floatingInput">Direccion de la revista </label>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-floating">
                                                <input type="number" name="telefono_rev" id="telefono_rev" value="<?php echo $revistaEditar->telefono_rev ?>" class="form-control" placeholder="Telefono  de la revista" required="">
                                                
                                                <label for="floatingInput">Telefono  de la revista</label>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-floating">
                                                <input type="text" name="correo_rev" id="correo_rev"  value="<?php echo $revistaEditar->correo_rev ?>" class="form-control" placeholder="Correo de la revista" required="">
                                                
                                                <label for="floatingInput">Correo de la revista</label>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-floating">
                                                <select name="fk_id_com" id="fk_id_com"  class="form-control">
                                                   <?php if ($comites): ?>
                                                    
                                                        <?php foreach ($comites as $c): ?>
                                                            <option value="<?php echo $c->id_com ?>"> <?php echo $c->nombre_com ?></option>

                                                        <?php endforeach;?>
                                                   <?php else :?>
                                                    <h1>No hay datos</h1>
                                                   <?php endif; ?>
                                                </select>
                                                <label for="floatingInput">Comite</label>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-floating">
                                                 <select name="fk_id_col" id="fk_id_col"  class="form-control">
                                                   <?php if ($colaboradores): ?>
                                                    
                                                        <?php foreach ($colaboradores as $c): ?>
                                                            <option value="<?php echo $c->id_col ?>"> <?php echo $c->nombres_col ?></option>

                                                        <?php endforeach;?>
                                                   <?php else :?>
                                                    <h1>No hay datos</h1>
                                                   <?php endif; ?>
                                                </select>
                                                <label for="floatingInput">Colaborador</label>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-12 ms-auto">
                                        <button type="submit" class="form-control">Guardar</button>
                                    </div>

                                </div>
                            </form>
                        </div>

                    
                    </div>
                </div>
            </section>

<script>
    $("#fk_id_com").val('<?php echo $revistaEditar->fk_id_com; ?>');
    $("#fk_id_col").val('<?php echo $revistaEditar->fk_id_col; ?>');
</script>