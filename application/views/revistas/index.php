<header class="site-header d-flex flex-column justify-content-center align-items-center">
                <div class="container">
                    <div class="row align-items-center">

                        <div class="col-lg-5 col-12">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">

                                    <li class="breadcrumb-item active"><a href="<?php echo site_url('revistas/nuevo');?>">Nueva Revista</a></li>


                                </ol>
                            </nav>

                            <h2 class="text-white">Listado de las Revistas</h2>
                        </div>

                    </div>
                </div>
            </header>
<section class="section-padding">
    <div class="container">
        <div class="row">
            <?php if ($revistas) :?>
                <table id="tblComite">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Nombre de Revista</th>
                            <th>Dirrección de Revista</th>
                            <th>Teléfono Revista</th>
                            <th>Correo Revista</th>
                            <th>Id Comite</th>
                            <th>Id Colaborador</th>
                            <th>Acciones</th>
                        </tr>

                    </thead>
                    <tbody>
                        <?php foreach ($revistas as $filaTemporal ): ?>
                            <tr>

                                <td class="text-center"> <?php echo $filaTemporal->id_rev ?></td>

                                <td> <?php echo $filaTemporal->nombre_rev ?></td>
                                <td> <?php echo $filaTemporal->direccion_rev ?></td>
                                <td> <?php echo $filaTemporal->telefono_rev ?></td>
                                <td> <?php echo $filaTemporal->correo_rev ?></td>
                                <td> <?php echo $filaTemporal->fk_id_com ?></td>
                                <td> <?php echo $filaTemporal->fk_id_col ?></td>
                                <td class="text-center">
                                <a href="<?php echo site_url(); ?>/revistas/editar/<?php echo $filaTemporal->id_rev; ?>" title="Editar " >
                                    <i class="mdi  mdi-pencil">Editar</i>
                                </a>
                                &nbsp;&nbsp;


                                    <a href="<?php echo site_url(); ?>/revistas/eliminar/<?php echo $filaTemporal->id_rev; ?>" title="Borrar " style="color:red" onclick="return confirm('Esta seguro de borra el registro?');">
                                    <i class="mdi  mdi-close">Eliminar</i>
                                    </a>

                                </td>

                            </tr>

                        <?php endforeach; ?>
                    </tbody>
                </table>
            <?php else: ?>
                <h1>No hay datos</h1>
            <?php endif;?>

        </div>
    </div>
</section>

<script type="text/javascript">
    $("#tblComite")
    .DataTable();
</script>
