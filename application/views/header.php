<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Articulos</title>
    <!-- CSS FILES -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@500;600;700&family=Open+Sans&display=swap" rel="stylesheet">
    <link href="<?php echo base_url(); ?>plantilla/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>plantilla/css/bootstrap-icons.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>plantilla/css/templatemo-topic-listing.css" rel="stylesheet">
    <!--Importacion de jquey-->
    <script src="https://code.jquery.com/jquery-3.7.0.min.js" integrity="sha256-2Pmvv0kuTBOenSvLm6bvfBSSHrUJ+3A7x6P5Ebd07/g=" crossorigin="anonymous"></script>



    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
    
    <!-- Importacion de datatables-->
    <link rel="stylesheet" href="https://cdn.datatables.net/1.13.4/css/jquery.dataTables.min.css">
    <script type="text/javascript" src="https://cdn.datatables.net/1.13.4/js/jquery.dataTables.min.js"></script>
    <!--
    TemplateMo 590 topic listing
    https://templatemo.com/tm-590-topic-listing
    -->
</head>

<body id="top">
    <main>
        <nav class="navbar navbar-expand-lg">
            <div class="container">
                <a class="navbar-brand" href="index.html">
                    <i class="bi-back"></i>
                    <span style="color: black;">Artículos</span>
                </a>
                <div class="d-lg-none ms-auto me-4">
                    <a href="#top" class="navbar-icon bi-person smoothscroll" style="color: black;"></a>
                </div>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarNav">
                    <ul class="navbar-nav ms-lg-5 me-lg-auto">
                        <li class="nav-item">
                            <a class="nav-link click-scroll" href="<?php echo site_url(''); ?>" style="color: black;">Inicio</a>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarLightDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false" style="color: black;">Autores</a>
                            <ul class="dropdown-menu dropdown-menu-light" aria-labelledby="navbarLightDropdownMenuLink">
                                <li> <a class="dropdown-item" href="<?php echo site_url('autores/index'); ?>" style="color: black;">Listado</a></li>
                                <li> <a class="dropdown-item" href="<?php echo site_url('autores/nuevo'); ?>" style="color: black;">Nuevo</a></li>
                            </ul>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarLightDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false" style="color: black;">Colaboradores</a>
                            <ul class="dropdown-menu dropdown-menu-light" aria-labelledby="navbarLightDropdownMenuLink">
                                <li> <a class="dropdown-item" href="<?php echo site_url('colaboradores/index'); ?>" style="color: black;">Listado</a></li>
                                <li> <a class="dropdown-item" href="<?php echo site_url('colaboradores/nuevo'); ?>" style="color: black;">Nuevo</a></li>
                            </ul>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarLightDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false" style="color: black;">Revista</a>
                            <ul class="dropdown-menu dropdown-menu-light" aria-labelledby="navbarLightDropdownMenuLink">
                                <li> <a class="dropdown-item" href="<?php echo site_url('revistas/index'); ?>" style="color: black;">Listado</a></li>
                                <li> <a class="dropdown-item" href="<?php echo site_url('revistas/nuevo'); ?>" style="color: black;">Nuevo</a></li>
                            </ul>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarLightDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false" style="color: black;">Comite</a>
                            <ul class="dropdown-menu dropdown-menu-light" aria-labelledby="navbarLightDropdownMenuLink">
                                <li> <a class="dropdown-item" href="<?php echo site_url('comites/index'); ?>" style="color: black;">Listado</a></li>
                                <li> <a class="dropdown-item" href="<?php echo site_url('comites/nuevo'); ?>" style="color: black;">Nuevo</a></li>
                            </ul>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarLightDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false" style="color: black;">Articulos</a>
                            <ul class="dropdown-menu dropdown-menu-light" aria-labelledby="navbarLightDropdownMenuLink">
                                <li> <a class="dropdown-item" href="<?php echo site_url('articulos/index'); ?>" style="color: black;">Listado</a></li>
                                <li> <a class="dropdown-item" href="<?php echo site_url('articulos/nuevo'); ?>" style="color: black;">Nuevo</a></li>
                            </ul>
                        </li>
                    </ul>
                    <div class="d-none d-lg-block">
                        <a href="#top" class="navbar-icon bi-person smoothscroll" style="color: black;"></a>
                    </div>
                </div>
            </div>
        </nav>
    </main>
</body>
</html>
