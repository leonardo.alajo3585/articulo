<section class="section-padding section-bg">
                <div class="container">
                    <div class="row">

                        <div class="col-lg-12 col-12">
                            <h3 class="mb-4 pb-2">Datos del Colaborador</h3>
                        </div>

                        <div class="col-lg-12 col-12">
                            <form  method="post" class="custom-form contact-form" role="form" action="<?php echo site_url(); ?>/colaboradores/procesoActualizar">
                                <div class="row">
                                    <input type="hidden" name="id_col" id="id_col" value="<?php echo $colaboradorEditar->id_col ?>" >
                                    <div class="col-lg-4 col-md-12 col-12">
                                        <div class="form-floating">
                                            <input type="text" name="nombres_col" id="nombres_col" class="form-control"  value="<?php echo $colaboradorEditar->nombres_col ?>" placeholder="Nombre del Colaborador" required="">
                                            
                                            <label for="floatingInput">Nombres del Colaborador </label>
                                        </div>
                                      
                                    </div>

                                    <div class="col-lg-4">
                                        <div class="form-floating">
                                                <input type="text" name="apellidos_col" id="apellidos_col"  value="<?php echo $colaboradorEditar->apellidos_col ?>"  class="form-control" placeholder="Apellidos  del Colaborador" required="">
                                                
                                                <label for="floatingInput">Apellidos  del Colaborador</label>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-floating">
                                                <input type="number" name="telefono_col" id="telefono_col" value="<?php echo $colaboradorEditar->telefono_col ?>"  class="form-control" placeholder="Teléfono" required="">
                                                
                                                <label for="floatingInput">Telefono  del Colaborador</label>
                                        </div>
                                    </div>

                                    <div class="col-lg-4 col-12 ms-auto">
                                        <button type="submit" class="form-control">Editar</button>
                                    </div>

                                </div>
                            </form>
                        </div>

                    
                    </div>
                </div>
            </section>