<section class="section-padding section-bg">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-12">
                <h3 class="mb-4 pb-2">Datos del Colaborador</h3>
            </div>
            <div class="col-lg-12 col-12">
                <form method="post" class="custom-form contact-form" role="form" action="<?php echo site_url(); ?>/colaboradores/guardar" id="form-colaborador">
                    <div class="row">
                        <div class="col-lg-4 col-md-12 col-12">
                            <div>
                                <input type="text" name="nombres_col" id="nombres_col" class="form-control" placeholder="Nombre del Colaborador" required="">
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div>
                                <input type="text" name="apellidos_col" id="apellidos_col" class="form-control" placeholder="Apellidos  del Colaborador" required="">
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div>
                                <input type="number" name="telefono_col" id="telefono_col" class="form-control" placeholder="Teléfono" required="">
                            </div>
                        </div>
                        <div class="col-lg-4 col-12 ms-auto">
                            <button type="submit" class="form-control">Guardar</button>
                        </div>
                    </div>
                </form>
            </div>
            <!-- Script de validación -->
            <script>
                $(document).ready(function () {
                    // Inicializar las validaciones del formulario
                    $("#form-colaborador").validate({
                        // Definir las reglas de validación para cada campo
                        rules: {
                            nombres_col: "required",
                            apellidos_col: "required",
                            telefono_col: {
                                required: true,
                                digits: true
                            }
                        },
                        // Definir los mensajes de error para cada campo
                        messages: {
                            nombres_col: {
                                required: "<span style='color: red;'>Por favor, ingrese el nombre del colaborador</span>"
                            },
                            apellidos_col: {
                                required: "<span style='color: red;'>Por favor, ingrese los apellidos del colaborador</span>"
                            },
                            telefono_col: {
                                required: "<span style='color: red;'>Por favor, ingrese el número de teléfono</span>",
                                digits: "<span style='color: red;'>Por favor, ingrese solo dígitos en el número de teléfono</span>"
                            }
                        },
                        // Definir el lugar donde se mostrarán los mensajes de error
                        errorPlacement: function(error, element) {
                            error.appendTo(element.parent());
                        }
                    });
                });
            </script>
        </div>
    </div>
</section>
