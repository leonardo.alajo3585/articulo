<header class="site-header d-flex flex-column justify-content-center align-items-center">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-5 col-12">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item active"><a href="<?php echo site_url('colaboradores/nuevo');?>">Nuevo Colaborador</a></li>
                    </ol>
                </nav>
                <h2 class="text-white">Listado de Colaboradores</h2>
            </div>
        </div>
    </div>
</header>

<section class="section-padding">
    <div class="container">
        <div class="row">
            <?php if ($colaboradores) :?>
                <table id="tblComite">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Nombre de Colaborador</th>
                            <th>Apellidos de Colaborador</th>
                            <th>Teléfono de Colaborador</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($colaboradores as $filaTemporal ): ?>
                            <tr>
                                <td class="text-center"><?php echo $filaTemporal->id_col ?></td>
                                <td><?php echo $filaTemporal->nombres_col ?></td>
                                <td><?php echo $filaTemporal->apellidos_col ?></td>
                                <td><?php echo $filaTemporal->telefono_col ?></td>
                                <td class="text-center">
                                    <a href="<?php echo site_url(); ?>/colaboradores/editar/<?php echo $filaTemporal->id_col; ?>" title="Editar">
                                        <i class="mdi mdi-pencil">Editar</i>
                                    </a>
                                    &nbsp;&nbsp;
                                    <a href="<?php echo site_url(); ?>/colaboradores/eliminar/<?php echo $filaTemporal->id_col; ?>" title="Borrar" style="color:red" onclick="return confirm('¿Está seguro de borrar el registro?');">
                                        <i class="mdi mdi-close">Eliminar</i>
                                    </a>
                                    &nbsp;&nbsp;
                                    <a href="<?php echo site_url('colaboradores/descargar_pdf/') . $filaTemporal->id_col; ?>" title="Descargar PDF">
                                        <i class="mdi mdi-file-pdf">Descargar PDF</i>
                                    </a>

                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            <?php else: ?>
                <h1>No hay datos</h1>
            <?php endif;?>
        </div>
    </div>
</section>

<script type="text/javascript">
    $(document).ready(function() {
        $('#tblComite').DataTable({
            dom: 'Bfrtip',
            buttons: [
                'pdf'
            ]
        });
    });

    function descargarPDF(id_col) {
        window.location.href = "<?php echo site_url('colaboradores/descargar_pdf/'); ?>" + id_col;
    }
</script>
