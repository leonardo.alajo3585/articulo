<section class="section-padding section-bg">
                <div class="container">
                    <div class="row">

                        <div class="col-lg-12 col-12">
                            <h3 class="mb-4 pb-2">Datos del Autor</h3>
                        </div>

                        <div class="col-lg-12 col-12">
                            <form  method="post" class="custom-form contact-form" role="form" action="<?php echo site_url(); ?>/autores/procesoActualizar">
                                <div class="row">
                                    <input type="hidden" name="id_aut" id="id_aut" value="<?php echo $autorEditar->id_aut ?>" >

                                    <div class="col-lg-4 col-md-12 col-12">
                                        <div class="form-floating">
                                            <input type="text" name="nombres_aut" id="nombres_aut" value="<?php echo $autorEditar->nombres_aut ?>" class="form-control" placeholder="Nombres" required="">
                                            
                                            <label for="floatingInput">Nombres</label>
                                        </div>
                                      
                                    </div>

                                    <div class="col-lg-4">
                                        <div class="form-floating">
                                                <input type="ematextil" name="apellidos_aut" id="apellidos_aut" value="<?php echo $autorEditar->apellidos_aut ?>"   class="form-control" placeholder="Apellidos" required="">
                                                
                                                <label for="floatingInput">Apellidos</label>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-floating">
                                                <input type="text" name="cedula_aut" id="cedula_aut"  value="<?php echo $autorEditar->cedula_aut ?>"   class="form-control" placeholder="Cedula" required="">
                                                
                                                <label for="floatingInput">Cedula</label>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-floating">
                                                <input type="number" name="telefono_aut" id="telefono_aut"  value="<?php echo $autorEditar->telefono_aut ?>"  class="form-control" placeholder="Telélefono" required="">
                                                
                                                <label for="floatingInput">Teléfono</label>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-floating">
                                                <input type="text" name="direccion_aut" id="direccion_aut"  value="<?php echo $autorEditar->direccion_aut ?>"  class="form-control" placeholder="Direccion" required="">
                                                
                                                <label for="floatingInput">Direccion</label>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-floating">
                                                <input type="email" name="correo_aut" id="correo_aut"  value="<?php echo $autorEditar->correo_aut ?>"  class="form-control" placeholder="Correo" required="">
                                                
                                                <label for="floatingInput">Correo</label>
                                        </div>
                                    </div>

                                    <div class="col-lg-4 col-12 ms-auto">
                                        <button type="submit" class="form-control">Guardar</button>
                                    </div>

                                </div>
                            </form>
                        </div>

                    
                    </div>
                </div>
            </section>