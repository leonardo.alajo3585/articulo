<header class="site-header d-flex flex-column justify-content-center align-items-center">
                <div class="container">
                    <div class="row align-items-center">

                        <div class="col-lg-5 col-12">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">

                                  <li class="breadcrumb-item active"><a href="<?php echo site_url('autores/nuevo');?>" style="color: white; font-weight: bold;">Ingrese un Nuevo Autor</a></li>

                                </ol>
                            </nav>

                            <h2 class="text-white">Listado de Autores </h2>
                        </div>

                    </div>
                </div>
            </header>
<section class="section-padding">
    <div class="container">
        <div class="row">
            <?php if ($autores) :?>
                <table id="tblAutores">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Nombres</th>
                            <th>Apellidos</th>
                            <th>Cedula</th>
                            <th>Teléfono</th>
                            <th>Direccion</th>
                            <th>Correo</th>
                            <th>Acciones</th>
                        </tr>

                    </thead>
                    <tbody>
                        <?php foreach ($autores as $filaTemporal ): ?>
                            <tr>

                                <td class="text-center"> <?php echo $filaTemporal->id_aut ?></td>

                                <td> <?php echo $filaTemporal->nombres_aut ?></td>
                                <td> <?php echo $filaTemporal->apellidos_aut ?></td>
                                <td> <?php echo $filaTemporal->cedula_aut ?></td>
                                <td> <?php echo $filaTemporal->telefono_aut ?></td>
                                <td> <?php echo $filaTemporal->direccion_aut ?></td>
                                <td> <?php echo $filaTemporal->correo_aut ?></td>
                                <td class="text-center">
                                <a href="<?php echo site_url(); ?>/autores/editar/<?php echo $filaTemporal->id_aut; ?>" title="Editar Autor" >
                                    <i class="mdi  mdi-pencil">Editar</i>
                                </a>
                                &nbsp;&nbsp;


                                    <a href="<?php echo site_url(); ?>/autores/eliminar/<?php echo $filaTemporal->id_aut; ?>" title="Borrar " style="color:red" onclick="return confirm('Esta seguro de borra el registro?');">
                                    <i class="mdi  mdi-close">Eliminar</i>
                                    </a>

                                </td>

                            </tr>

                        <?php endforeach; ?>
                    </tbody>
                </table>
            <?php else: ?>
                <h1>No hay datos</h1>
            <?php endif;?>

        </div>
    </div>
</section>

<script type="text/javascript">
    $("#tblAutores")
    .DataTable();

</script>
