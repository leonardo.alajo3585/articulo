<section class="section-padding section-bg">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-12">
                <h3 class="mb-4 pb-2">Datos del Autor</h3>
            </div>
            <div class="col-lg-12 col-12">
                <form method="post" class="custom-form contact-form" role="form" action="<?php echo site_url(); ?>/autores/guardar" id="form-autor">
                    <div class="row">
                        <div class="col-lg-4 col-md-12 col-12">
                            <div>
                                <input type="text" name="nombres_aut" id="nombres_aut" class="form-control" placeholder="Nombres" required="">
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div>
                                <input type="text" name="apellidos_aut" id="apellidos_aut" class="form-control" placeholder="Apellidos" required="">
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div>
                                <input type="text" name="cedula_aut" id="cedula_aut" class="form-control" placeholder="Cédula" required="">
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div>
                                <input type="number" name="telefono_aut" id="telefono_aut" class="form-control" placeholder="Teléfono" required="">
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div>
                                <input type="text" name="direccion_aut" id="direccion_aut" class="form-control" placeholder="Dirección" required="">
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div>
                                <input type="email" name="correo_aut" id="correo_aut" class="form-control" placeholder="Correo" required="">
                            </div>
                        </div>
                        <div class="col-lg-4 col-12 ms-auto">
                            <button type="submit" class="form-control">Guardar</button>
                        </div>
                    </div>
                </form>
            </div>
            <!-- Script de validación -->
            <script>
                $(document).ready(function () {
                    // Inicializar las validaciones del formulario
                    $("#form-autor").validate({
                        // Definir las reglas de validación para cada campo
                        rules: {
                            nombres_aut: {
                                required: true,
                                lettersonly: true
                            },
                            apellidos_aut: {
                                required: true,
                                lettersonly: true
                            },
                            cedula_aut: "required",
                            telefono_aut: {
                                required: true,
                                digits: true
                            },
                            direccion_aut: "required",
                            correo_aut: {
                                required: true,
                                email: true
                            }
                        },
                        // Definir los mensajes de error para cada campo
                        messages: {
                            nombres_aut: {
                                required: "<span style='color:red;'>Por favor, ingrese el nombre del autor</span>",
                                lettersonly: "<span style='color:red;'>Por favor, ingrese solo letras</span>"
                            },
                            apellidos_aut: {
                                required: "<span style='color:red;'>Por favor, ingrese los apellidos del autor</span>",
                                lettersonly: "<span style='color:red;'>Por favor, ingrese solo letras</span>"
                            },
                            cedula_aut: "<span style='color:red;'>Por favor, ingrese el número de cédula</span>",
                            telefono_aut: {
                                required: "<span style='color:red;'>Por favor, ingrese el número de teléfono</span>",
                                digits: "<span style='color:red;'>Por favor, ingrese solo dígitos</span>"
                            },
                            direccion_aut: "<span style='color:red;'>Por favor, ingrese la dirección</span>",
                            correo_aut: {
                                required: "<span style='color:red;'>Por favor, ingrese el correo electrónico</span>",
                                email: "<span style='color:red;'>Por favor, ingrese un correo electrónico válido</span>"
                            }
                        },
                        // Definir el lugar donde se mostrarán los mensajes de error
                        errorPlacement: function(error, element) {
                            error.appendTo(element.parent());
                        }
                    });
                });
            </script>
        </div>
    </div>
</section>
