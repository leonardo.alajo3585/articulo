<footer class="site-footer section-padding">
            <div class="container">
                <div class="row">

                    <div class="col-lg-3 col-12 mb-4 pb-2">
                        <a class="navbar-brand mb-2" href="index.html">
                            <i class="bi-back"></i>
                            <span>Artículos</span>
                        </a>
                    </div>



                    <div class="col-lg-3 col-md-4 col-6 mb-4 mb-lg-0">
                        <h6 class="site-footer-title mb-3">Contactos</h6>

                        <p class="text-black d-flex mb-1">
                            <a href="tel: 305-240-9671" class="color: #000000;">
                                0995728298 - 032-411840
                            </a>
                        </p>

                        <p class="text-white d-flex">
                            <a href="mailto:in.com" class="color: #000000;">
                                articulo@ejemplo.com
                            </a>
                        </p>
                    </div>

                    <div class="col-lg-3 col-md-4 col-12 mt-4 mt-lg-0 ms-auto">
                        <div class="dropdown">
                            <ul class="dropdown-menu">
                                <li><button class="dropdown-item" type="button">Thai</button></li>

                                <li><button class="dropdown-item" type="button">Myanmar</button></li>

                                <li><button class="dropdown-item" type="button">Arabic</button></li>
                            </ul>
                        </div>

                        <p class="copyright-text mt-lg-5 mt-4"><strong style="color:black;">TODOS LOS DERECHOS RESERVADOS.</strong></p>
                        <br><br><strong style="color:black;">PROPIETARIOS:</strong><br><br><br>
                        <a rel="nofollow" href="https://templatemo.com" target="_blank">CALUÑA, ALAJO, LLAMBO, CONSTANTE</a>  <a href="https://themewagon.com"> </a>  </p>

                    </div>

                </div>
            </div>
        </footer>


        <!-- JAVASCRIPT FILES -->
        <script src="<?php base_url();?>plantilla/js/jquery.min.js"></script>
        <script src="<?php base_url();?>plantilla/js/bootstrap.bundle.min.js"></script>
        <script src="<?php base_url();?>plantilla/js/jquery.sticky.js"></script>
        <script src="<?php base_url();?>plantilla/js/click-scroll.js"></script>
        <script src="<?php base_url();?>plantilla/js/custom.js"></script>

    </body>
</html>
