<header class="site-header d-flex flex-column justify-content-center align-items-center">
                <div class="container">
                    <div class="row align-items-center">

                        <div class="col-lg-5 col-12">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                
                                    <li class="breadcrumb-item active"><a href="<?php echo site_url('articulos/nuevo');?>">Nuevo artículos</a></li>
                                    
                                    
                                </ol>
                            </nav>

                            <h2 class="text-white">Listado de Articulos</h2>
                        </div>

                    </div>
                </div>
            </header>
<section class="section-padding">
    <div class="container">
        <div class="row">
            <?php if ($articulos) :?>
                <table id="tblComite">
                    <thead>
                        <tr> 
                            <th>ID</th>
                            <th>Tema </th>
                            <th>Fecha</th>
                            <th>Autores</th>
                            <th>Revista</th>
                            <th>Volumenes</th>
                            <th>Palabras Claves</th>                           
                            <th>Resumen</th>                         
                            <th>Acciones</th>
                        </tr>

                    </thead>
                    <tbody> 
                        <?php foreach ($articulos as $filaTemporal ): ?>
                            <tr>

                                <td class="text-center"> <?php echo $filaTemporal->id_art ?></td>
                            
                                <td> <?php echo $filaTemporal->tema_art ?></td>
                                <td> <?php echo $filaTemporal->fecha_art ?></td>
                                <td> <?php echo $filaTemporal->fk_id_aut ?></td>
                                <td> <?php echo $filaTemporal->fk_id_rev ?></td>
                                <td> <?php echo $filaTemporal->volumen_art ?></td>
                                <td> <?php echo $filaTemporal->palabras_claves_art ?></td>
                                <td> <?php echo $filaTemporal->resumen_art ?></td>
                                
                                <td class="text-center">
                                <a href="<?php echo site_url(); ?>/articulos/editar/<?php echo $filaTemporal->id_art; ?>" title="Editar " >
                                    <i class="mdi  mdi-pencil">Editar</i>
                                </a>
                                &nbsp;&nbsp;

                              
                                    <a href="<?php echo site_url(); ?>/articulos/eliminar/<?php echo $filaTemporal->id_art; ?>" title="Borrar " style="color:red" onclick="return confirm('Esta seguro de borra el registro?');">
                                    <i class="mdi  mdi-close">Eliminar</i>
                                    </a>
                              
                                </td>

                            </tr>

                        <?php endforeach; ?>
                    </tbody>
                </table>
            <?php else: ?>
                <h1>No hay datos</h1>
            <?php endif;?>
            
        </div>  
    </div>
</section>

<script type="text/javascript">
    $("#tblComite")
    .DataTable();
</script>
