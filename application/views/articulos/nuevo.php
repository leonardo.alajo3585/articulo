<section class="section-padding section-bg">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-12">
                <h3 class="mb-4 pb-2">Datos de artículo</h3>
            </div>
            <div class="col-lg-12 col-12">
                <form method="post" class="custom-form contact-form" role="form" action="<?php echo site_url(); ?>/articulos/guardar" id="form-articulo">
                    <div class="row">
                        <div class="col-lg-4 col-md-12 col-12">
                            <div class="form-group">
                                <input type="text" name="tema_art" id="tema_art" class="form-control" placeholder="Tema del Artículo">
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="form-group">
                                <input type="date" name="fecha_art" id="fecha_art" class="form-control" placeholder="Fecha de Artículo">
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="form-group">
                                <select name="fk_id_aut" id="fk_id_aut" class="form-control">
                                    <?php if ($autores): ?>
                                        <?php foreach ($autores as $c): ?>
                                            <option value="<?php echo $c->id_aut ?>"> <?php echo $c->nombres_aut ?> <?php echo $c->apellidos_aut ?> </option>
                                        <?php endforeach;?>
                                    <?php else :?>
                                        <option value="">No hay datos</option>
                                    <?php endif; ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="form-group">
                                <select name="fk_id_rev" id="fk_id_rev" class="form-control">
                                    <?php if ($revistas): ?>
                                        <?php foreach ($revistas as $c): ?>
                                            <option value="<?php echo $c->id_rev ?>"> <?php echo $c->nombre_rev ?> </option>
                                        <?php endforeach;?>
                                    <?php else :?>
                                        <option value="">No hay datos</option>
                                    <?php endif; ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="form-group">
                                <input type="number" name="volumen_art" id="volumen_art" class="form-control" placeholder="Volúmenes del artículo">
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="form-group">
                                <input type="text" name="palabras_claves_art" id="palabras_claves_art" class="form-control" placeholder="Palabras Claves">
                            </div>
                        </div>
                        <div class="col-lg-3"></div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <input type="text" name="resumen_art" id="resumen_art" class="form-control" placeholder="Resumen">
                            </div>
                        </div>
                        <div class="col-lg-3"></div>
                        <div class="col-lg-4 col-12 ms-auto">
                            <button type="submit" class="form-control">Guardar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>

<!-- Script de validación -->
<script>
    $(document).ready(function () {
        // Inicializar las validaciones del formulario
        $("#form-articulo").validate({
            // Definir las reglas de validación para cada campo
            rules: {
                tema_art: "required",
                fecha_art: "required",
                fk_id_aut: "required",
                fk_id_rev: "required",
                volumen_art: "required",
                palabras_claves_art: "required",
                resumen_art: "required"
            },
            // Definir los mensajes de error para cada campo
            messages: {
                tema_art: "Por favor, ingrese el tema del artículo",
                fecha_art: "Por favor, ingrese la fecha del artículo",
                fk_id_aut: "Por favor, seleccione un autor",
                fk_id_rev: "Por favor, seleccione una revista",
                volumen_art: "Por favor, ingrese el volumen del artículo",
                palabras_claves_art: "Por favor, ingrese las palabras clave del artículo",
                resumen_art: "Por favor, ingrese el resumen del artículo"
            },
            // Definir el lugar donde se mostrarán los mensajes de error
            errorPlacement: function (error, element) {
                error.appendTo(element.parent());
                error.addClass('text-danger'); // Agregar la clase para el color rojo
            }
        });
    });
</script>
