<section class="section-padding section-bg">

                <div class="container">
                    <div class="row">

                        <div class="col-lg-12 col-12">
                            <h3 class="mb-4 pb-2">Datos del articulo</h3>
                        </div>

                        <div class="col-lg-12 col-12">
                            <form  method="post" class="custom-form contact-form" role="form" action="<?php echo site_url(); ?>/articulos/procesoActualizar">
                                <div class="row">
                                    <input type="hidden" name="id_art" id="id_art" value="<?php echo $articuloEditar->id_art ?>" >

                                    <div class="col-lg-4 col-md-12 col-12">
                                        <div class="form-floating">
                                            <input type="text" name="tema_art" id="tema_art" value="<?php echo $articuloEditar->tema_art ?>"   class="form-control" placeholder="Tema del Artículo " required="">

                                            <label for="floatingInput">Tema del Artículo </label>
                                        </div>

                                    </div>

                                    <div class="col-lg-4">
                                        <div class="form-floating">
                                                <input type="date" name="fecha_art" id="fecha_art"  value="<?php echo $articuloEditar->fecha_art ?>"  class="form-control" placeholder="Direccion de la revista" required="">

                                                <label for="floatingInput">Fecha de Artículo </label>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-floating">

                                                <select name="fk_id_aut" id="fk_id_aut"  class="form-control">
                                                   <?php if ($autores): ?>

                                                        <?php foreach ($autores as $c): ?>
                                                            <option value="<?php echo $c->id_aut ?>"> <?php echo $c->nombres_aut ?> <?php echo $c->apellidos_aut ?> </option>

                                                        <?php endforeach;?>
                                                   <?php else :?>
                                                    <h1>No hay datos</h1>
                                                   <?php endif; ?>
                                                </select>
                                                <label for="floatingInput">Autor de la Revista</label>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-floating">

                                                <select name="fk_id_rev" id="fk_id_rev"  class="form-control">
                                                   <?php if ($revistas): ?>

                                                        <?php foreach ($revistas as $c): ?>
                                                            <option value="<?php echo $c->id_rev ?>"> <?php echo $c->nombre_rev ?> </option>

                                                        <?php endforeach;?>
                                                   <?php else :?>
                                                    <h1>No hay datos</h1>
                                                   <?php endif; ?>
                                                </select>

                                                <label for="floatingInput">Nombre de la revista</label>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-floating">

                                                <input type="number" name="volumen_art" id="volumen_art"  value="<?php echo $articuloEditar->volumen_art ?>" class="form-control" placeholder="Volúmenes del articulo" required="">
                                                <label for="floatingInput">Volúmenes del articulo</label>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-floating">
                                                <input type="text" name="palabras_claves_art" id="palabras_claves_art"  value="<?php echo $articuloEditar->palabras_claves_art ?>"  class="form-control" placeholder="Palabras Claves" required="">
                                                <label for="floatingInput">Palabras Claves</label>
                                        </div>
                                    </div>
                                    <div class="col-lg-3">

                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-floating">
                                                <input type="text" name="resumen_art" id="resumen_art"  class="form-control" value="<?php echo $articuloEditar->resumen_art ?>" placeholder="Resumen" required="">
                                                <label for="floatingInput">Resumen</label>
                                        </div>
                                    </div>
                                    <div class="col-lg-3">

                                    </div>
                                    <div class="col-lg-4 col-12 ms-auto">
                                        <button type="submit" class="form-control">Guardar</button>
                                    </div>

                                </div>
                            </form>
                        </div>


                    </div>
                </div>
            </section>

<script>
    $("#fk_id_aut").val('<?php echo $articuloEditar->fk_id_aut; ?>');
    $("#fk_id_rev").val('<?php echo $articuloEditar->fk_id_rev; ?>');
</script>
