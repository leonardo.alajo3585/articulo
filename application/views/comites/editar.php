<section class="section-padding section-bg">
                <div class="container">
                    <div class="row">

                        <div class="col-lg-12 col-12">
                            <h3 class="mb-4 pb-2">Datos del Comite</h3>
                        </div>

                        <div class="col-lg-12 col-12">
                            <form  method="post" class="custom-form contact-form" role="form" action="<?php echo site_url(); ?>/comites/procesoActualizar">
                                <div class="row">
                                <input type="hidden" name="id_com" id="id_com" value="<?php echo $comiteEditar->id_com ?>" >

                                    <div class="col-lg-6 col-md-12 col-12">
                                        <div class="form-floating">
                                            <input type="text" name="nombre_com" id="nombre_com" value="<?php echo $comiteEditar->nombre_com ?>" class="form-control" placeholder="Nombre del comite" required="">
                                            
                                            <label for="floatingInput">Nombre del Comite </label>
                                        </div>
                                      
                                    </div>

                                    <div class="col-lg-6">
                                        <div class="form-floating">
                                                <input type="text" name="director_com" id="director_com"  value="<?php echo $comiteEditar->director_com ?>" class="form-control" placeholder="Nombre del Director del Comite" required="">
                                                
                                                <label for="floatingInput">Director del Comite</label>
                                        </div>
                                    </div>
                                  

                                    <div class="col-lg-4 col-12 ms-auto">
                                        <button type="submit" class="form-control">Editar</button>
                                    </div>

                                </div>
                            </form>
                        </div>

                    
                    </div>
                </div>
            </section>