<header class="site-header d-flex flex-column justify-content-center align-items-center">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-5 col-12">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item active"><a href="<?php echo site_url('comites/nuevo');?>">Nuevo Comite</a></li>
                    </ol>
                </nav>
                <h2 class="text-white">Lista de comites</h2>
            </div>
        </div>
    </div>
</header>

<section class="section-padding">
    <div class="container">
        <div class="row">
            <?php if (!empty($comites)) :?>
                <table id="tblComite">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Nombre</th>
                            <th>Director</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($comites as $filaTemporal): ?>
                            <tr>
                                <td class="text-center"><?php echo $filaTemporal->id_com ?></td>
                                <td><?php echo $filaTemporal->nombre_com ?></td>
                                <td><?php echo $filaTemporal->director_com ?></td>
                                <td class="text-center">
                                    <a href="<?php echo site_url(); ?>/comites/editar/<?php echo $filaTemporal->id_com; ?>" title="Editar" >
                                        <i class="mdi mdi-pencil">Editar</i>
                                    </a>
                                    &nbsp;&nbsp;
                                    <a href="<?php echo site_url(); ?>/comites/eliminar/<?php echo $filaTemporal->id_com; ?>" title="Borrar" style="color:red" onclick="return confirm('¿Está seguro de borrar el registro?');">
                                        <i class="mdi mdi-close">Eliminar</i>
                                    </a>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            <?php else: ?>
                <h1>No hay datos</h1>
            <?php endif;?>
        </div>
    </div>
</section>

<script type="text/javascript">
    $("#tblComite").DataTable();
</script>
