<section class="section-padding section-bg">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-12">
                <h3 class="mb-4 pb-2">Datos del Comité</h3>
            </div>
            <div class="col-lg-12 col-12">
                <form method="post" class="custom-form contact-form" role="form" action="<?php echo site_url(); ?>/comites/guardar" id="form-comite">
                    <div class="row">
                        <div class="col-lg-6 col-md-12 col-12">
                            <div class="form-group">
                                <input type="text" name="nombre_com" id="nombre_com" class="form-control" placeholder="Nombre del comité" required="">
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <input type="text" name="director_com" id="director_com" class="form-control" placeholder="Nombre del Director del Comité" required="">
                            </div>
                        </div>
                        <div class="col-lg-4 col-12 ms-auto">
                            <button type="submit" class="form-control">Guardar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>

<!-- Script de validación -->
<script>
    $(document).ready(function () {
        // Inicializar las validaciones del formulario
        $("#form-comite").validate({
            // Definir las reglas de validación para cada campo
            rules: {
                nombre_com: "required",
                director_com: "required"
            },
            // Definir los mensajes de error para cada campo
            messages: {
                nombre_com: "Por favor, ingrese el nombre del comité",
                director_com: "Por favor, ingrese el nombre del Director del Comité"
            },
            // Definir el lugar donde se mostrarán los mensajes de error
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
                error.addClass('text-danger'); // Agregar la clase para el color rojo
            }
        });
    });
</script>
